ARG VERSION=test
FROM zapps/python:${VERSION}

RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
RUN wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.1.85-1_amd64.deb && \
    dpkg -i cuda-repo-ubuntu1604_9.1.85-1_amd64.deb && \
    rm cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
RUN wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1604/x86_64/nvidia-machine-learning-repo-ubuntu1604_1.0.0-1_amd64.deb && \
    dpkg -i nvidia-machine-learning-repo-ubuntu1604_1.0.0-1_amd64.deb && \
    rm nvidia-machine-learning-repo-ubuntu1604_1.0.0-1_amd64.deb
RUN apt-get update && \
    apt-get -y install cuda9.0 cuda-cublas-9-0 cuda-cufft-9-0 cuda-curand-9-0 \
      cuda-cusolver-9-0 cuda-cusparse-9-0 libcudnn7=7.1.4.18-1+cuda9.0 \
      libnccl2=2.2.13-1+cuda9.0 cuda-command-line-tools-9-0 build-essential python3-dev libeigen3-dev htop cmake && \
    apt-get clean

RUN pip install --upgrade pip \
	    numpy \
            notebook \
            jupyterlab \
            tensorflow-gpu \
            tensorboard \
            pandas \
            matplotlib \
            scipy \
            seaborn \
            scikit-learn \
            scikit-image \
            sympy \
            cython \
            patsy \
            statsmodels \
            cloudpickle \
            dill \
            numba \
            bokeh \
            torch \
            torchvision 

RUN pip install eigency

# nvidia-docker
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=9.0"

COPY start_notebook.sh /usr/local/bin/start_notebook.sh
RUN chmod 755 /usr/local/bin/start_notebook.sh

